package com.silkchina.common.web.controller;

import com.silkchina.base.web.*;
import com.silkchina.common.*;
import com.silkchina.common.dto.*;
import com.silkchina.common.service.*;
import com.silkchina.common.web.request.*;
import org.springframework.stereotype.*;
import org.springframework.web.bind.annotation.*;

import javax.annotation.*;

/**
 * 用户管理
 * <p>
 * <li>根据id查询用户信息</li>
 * <li>新增用户</li>
 * <li>删除用户</li>
 * <li>修改用户</li>
 * <li>查询所有用户</li>
 * </p>
 *
 * @author Wang Jun
 */
@Controller
@RequestMapping("/user")
public class UserController extends BusinessResources {

    /**
     * User service.
     */
    @Resource
    UserService userService;

    /**
     * get By Id
     *
     * @param request the request
     * @return the string
     */
    @ResponseBody
    @RequestMapping(value = "/getById", method = RequestMethod.POST)
    public String getById(@ModelAttribute UserRequest request) {

        LOGGER.info(Constant.MODEL_NAME_USER + "getById,request={}", request);

        UserDTO userDTO = modelMapper.map(request, UserDTO.class);

        return userService.getUserById(userDTO);
    }

    /**
     * Save.
     *
     * @param request the request
     * @return the string
     */
    @ResponseBody
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String save(@ModelAttribute UserRequest request) {

        LOGGER.info(Constant.MODEL_NAME_USER + "save接口调用,request={}", request);

        UserDTO userDTO = modelMapper.map(request, UserDTO.class);

        return userService.save(userDTO);
    }

    /**
     * Del.
     *
     * @param request the request
     */
    @ResponseBody
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public void del(@ModelAttribute UserRequest request) {

        LOGGER.info(Constant.MODEL_NAME_USER + "del接口调用,request={}", request);

        UserDTO userDTO = modelMapper.map(request, UserDTO.class);

        userService.del(userDTO);
    }

    /**
     * Update.
     *
     * @param request the request
     */
    @ResponseBody
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public void update(@ModelAttribute UserRequest request) {

        LOGGER.info(Constant.MODEL_NAME_USER + "update接口调用,request={}", request);

        UserDTO userDTO = modelMapper.map(request, UserDTO.class);

        userService.update(userDTO);
    }

    /**
     * Get all.
     *
     * @return the all
     */
    @ResponseBody
    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    public String getAll() {

        LOGGER.info(Constant.MODEL_NAME_USER + "getAll接口调用");

        return userService.getAll();
    }

    @ResponseBody
    @RequestMapping(value = "/find", method = RequestMethod.POST)
    public String find(@ModelAttribute UserRequest request) {

        LOGGER.info(Constant.MODEL_NAME_USER + "find接口调用,request={}", request);

        UserDTO userDTO = modelMapper.map(request, UserDTO.class);

        return userService.find(userDTO, request.getPage(), request.getRows());
    }

}
