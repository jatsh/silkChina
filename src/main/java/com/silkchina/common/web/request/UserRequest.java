package com.silkchina.common.web.request;

import lombok.*;

@Data
@NoArgsConstructor
@ToString(callSuper = true)
public class UserRequest {

    private long id;

    /**
     * 用户昵称
     */
    private String userName;

    /**
     * 密码
     */
    private String password;

    /**
     * 性别
     */
    private String userSex;

    /**
     * 手机号
     */
    private String phoneNumber;

    /**
     * 公司id
     */
    private Long companyId;

    /**
     * 微信号
     */
    private String weChatId;

    /**
     * status
     */
    private String status;

    private int page = 0;

    private int rows = 10;

}
