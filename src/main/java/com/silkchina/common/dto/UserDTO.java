package com.silkchina.common.dto;

import lombok.*;

@Data
@NoArgsConstructor
@ToString(callSuper = true)
public class UserDTO {

    private long id;

    /**
     * 用户昵称
     */
    private String userName;

    /**
     * 密码
     */
    private String password;

    /**
     * 性别
     */
    private String userSex;

    /**
     * 手机号
     */
    private String phoneNumber;

    /**
     * 公司id
     */
    private Long companyId;

    /**
     * 微信号
     */
    private String weChatId;

    /**
     * status
     */
    private String status;

}
