package com.silkchina.common.service;

import com.alibaba.fastjson.*;
import com.baidu.unbiz.fluentvalidator.*;
import com.silkchina.base.service.*;
import com.silkchina.base.utils.*;
import com.silkchina.common.dal.entity.*;
import com.silkchina.common.dal.mapper.*;
import com.silkchina.common.dto.*;
import com.silkchina.common.enums.*;
import com.silkchina.common.validators.*;
import lombok.extern.slf4j.*;
import org.springframework.context.*;
import org.springframework.stereotype.*;

import javax.annotation.*;
import java.util.*;

import static com.baidu.unbiz.fluentvalidator.ResultCollectors.toSimple;


/**
 * The type User service.
 */
@Component
@Slf4j
public class UserService extends BaseService {

    /**
     * The User mapper.
     */
    @Resource
    UserMapper userMapper;

    /**
     * Gets user by id.
     *
     * @param userDTO the user dto
     * @return the user by id
     */
    public String getUserById(UserDTO userDTO) {

        User user = userMapper.getById(userDTO.getId());

        if (null != user) {
            return JSON.toJSONString(user);
        } else {
            return assembleFailResult("1000", "user not exist!");
        }
    }

    /**
     * Add user.
     *
     * @param userDTO the user dto
     * @return the string
     */
    public String save(UserDTO userDTO) {

        Result result = FluentValidator.checkAll().failOver()
                .on(userDTO, SpringContextHolder.getBean(UserCreateValidator.class))
                .doValidate()
                .result(toSimple());

        if (!result.isSuccess()) {
            return JSON.toJSONString(result);
        }

        User user = User.builder()
                .userName(userDTO.getUserName())
                .password(userDTO.getPassword())
                .userSex(userDTO.getUserSex())
                .phoneNumber(userDTO.getPhoneNumber())
                .companyId(userDTO.getCompanyId())
                .weChatId(userDTO.getWeChatId())
                .status(StatusEnum.VALID.getStatus())
                .build();

        userMapper.insert(user);

        return assembleSuccessResult(null);
    }

    /**
     * Del.
     *
     * @param userDTO the user dto
     */
    public void del(UserDTO userDTO) {

        User user = userMapper.getById(userDTO.getId());
        user.setStatus(StatusEnum.INVALID.getStatus());
        userMapper.update(user);
    }

    /**
     * Update.
     *
     * @param userDTO the user dto
     */
    public void update(UserDTO userDTO) {

        User user = userMapper.getById(userDTO.getId());
        user.setUserName(userDTO.getUserName());
        user.setUserSex(userDTO.getUserSex());
        user.setPassword(userDTO.getPassword());
        user.setPhoneNumber(userDTO.getPhoneNumber());
        user.setCompanyId(userDTO.getCompanyId());
        user.setWeChatId(userDTO.getWeChatId());

        userMapper.update(user);
    }

    /**
     * Get all.
     *
     * @return the all
     */
    public String getAll() {

        List userList = userMapper.findAll();
        return JSON.toJSONString(userList);
    }

    /**
     * Find.
     *
     * @param userDTO the user dto
     * @param page    the page
     * @param rows    the rows
     * @return the string
     */
    public String find(UserDTO userDTO, int page, int rows) {

        List userList = userMapper.find(userDTO.getUserName(),
                String.valueOf(userDTO.getCompanyId()),
                userDTO.getWeChatId(),
                userDTO.getStatus());

        return JSON.toJSONString(userList);
    }
}
