package com.silkchina.common.validators;

import com.baidu.unbiz.fluentvalidator.*;
import com.silkchina.common.dal.entity.*;
import com.silkchina.common.dal.mapper.*;
import com.silkchina.common.dto.*;
import com.silkchina.common.enums.*;
import org.apache.commons.lang3.*;
import org.springframework.stereotype.*;

import javax.annotation.*;

@Component
public class UserCreateValidator extends ValidatorHandler<UserDTO> implements Validator<UserDTO>{

    @Resource
    UserMapper userMapper;

    @Override
    public boolean validate(ValidatorContext context, UserDTO userDTO){

        boolean result = true;

        //基本参数校验
        if (!basicValidate(context,userDTO)){

            return false;
        }

        User user = userMapper.findByUserNameAndStatus(userDTO.getUserName(), StatusEnum.VALID.getStatus());

        if (null!=user){

            context.addErrorMsg("userName already exist!");
            result = false;
        }
        return result;
    }

    public boolean basicValidate(ValidatorContext context,UserDTO userDTO){

        boolean result = true;

        if (StringUtils.isBlank(userDTO.getUserName())){

            context.addErrorMsg("params error, userName is null");
            result = false;
        }

        if (StringUtils.isBlank(userDTO.getPassword())){

            context.addErrorMsg("params error, password is null");
            result = false;
        }

        if (StringUtils.isBlank(userDTO.getPhoneNumber())){

            context.addErrorMsg("params error, phoneNumber is null");
            result = false;
        }

        if (StringUtils.isBlank(userDTO.getUserSex())){

            context.addErrorMsg("params error, userSex is null");
            result = false;
        }

        if (StringUtils.isBlank(userDTO.getWeChatId())){

            context.addErrorMsg("params error, weChat_id is null");
            result = false;
        }

        if (null == userDTO.getCompanyId()){

            context.addErrorMsg("params error, companyId is null");
            result = false;
        }

        return result;

    }

}
