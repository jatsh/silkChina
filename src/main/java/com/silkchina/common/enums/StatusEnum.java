package com.silkchina.common.enums;

/**
 * 状态枚举类
 *
 * @author Wang Jun
 */
public enum StatusEnum {

    /**
     * On status enum.
     */
    VALID("1"),

    /**
     * Off status enum.
     */
    INVALID("0");

    private String status;

    StatusEnum(String status) {

        this.status = status;
    }

    /**
     * Get status string.
     *
     * @return the string
     */
    public String getStatus() {

        return this.status;
    }
}

