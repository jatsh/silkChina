package com.silkchina.common.dal.mapper;


import com.silkchina.base.mapper.*;
import com.silkchina.common.dal.entity.*;
import com.silkchina.common.dto.*;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.*;

import java.util.*;


/**
 * The interface User mapper.
 */
@Repository
public interface UserMapper extends GenericMapper<User> {

    List<UserDTO> find(@Param("userName") String userName,
                       @Param("companyId") String companyId,
                       @Param("weChatId") String weChatId,
                       @Param("status") String status);

    List<UserDTO> findByUserName(@Param("userName") String userName,
                                 @Param("status") String status);

    User findByUserNameAndStatus(@Param("userName") String userName,
                                 @Param("status") String status);


}
