package com.silkchina.common.dal.entity;

import com.silkchina.base.entity.*;
import lombok.*;

/**
 * The type User.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class User extends DataObjectBase {

    /**
     * 用户昵称
     */
    private String userName;

    /**
     * 密码
     */
    private String password;

    /**
     * 性别
     */
    private String userSex;

    /**
     * 手机号
     */
    private String phoneNumber;

    /**
     * 公司id
     */
    private Long companyId;

    /**
     * 微信号
     */
    private String weChatId;

    /**
     * status
     */
    private String status;

}