package com.silkchina.base.service;


import com.alibaba.fastjson.*;
import com.silkchina.base.result.*;
import org.slf4j.*;

import java.io.*;

public abstract class BaseService {

    protected Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    public BaseService() {
    }

    protected String assembleSuccessResult(Object model) {
        Result result = new Result();
        result.setSuccess(true);
        result.setModel((Serializable) model);
        return JSON.toJSONString(result);
    }

    protected String assembleFailResult(Object errorCode, String errorMessage) {
        return JSON.toJSONString(this.assembleFailResult(errorCode, errorMessage, (String) null));
    }

    protected String assembleFailResult(Object errorCode, String errorMessage, String desc) {
        Result result = new Result();
        result.setSuccess(false);
        result.setErrorCode(errorCode);
        result.setErrorMessage(errorMessage);
        return JSON.toJSONString(result);
    }
}
