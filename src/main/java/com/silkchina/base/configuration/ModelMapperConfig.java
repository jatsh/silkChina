package com.silkchina.base.configuration;

import org.modelmapper.*;
import org.springframework.context.annotation.*;

/**
 * The type Model mapper config.
 */
@Configuration
public class ModelMapperConfig {

    /**
     * Instantiates a new Model mapper config.
     */
    public ModelMapperConfig() {

    }

    /**
     * Model mapper model mapper.
     *
     * @return the model mapper
     */
    @Bean
    public ModelMapper modelMapper() {

        return new ModelMapper();

    }
}
