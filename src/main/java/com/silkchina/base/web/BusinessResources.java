package com.silkchina.base.web;

import com.silkchina.base.exception.*;
import com.silkchina.base.result.*;
import org.modelmapper.*;
import org.slf4j.*;

import javax.annotation.*;

public abstract class BusinessResources {

    protected Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Resource
    public ModelMapper modelMapper;

    public BusinessResources() {
    }

    protected Object parseResult(Result result) {
        if (!result.isSuccess()) {
            throw new RestServiceException(result);
        } else {
            return result.getModel();
        }
    }
}
