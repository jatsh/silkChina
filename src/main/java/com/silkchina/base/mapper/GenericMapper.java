package com.silkchina.base.mapper;


import com.silkchina.base.entity.*;
import org.apache.ibatis.annotations.*;

import java.util.*;

/**
 * @param <T>
 * @author Wang Jun
 */
public interface GenericMapper<T extends DataObjectBase> {

    void insert(T var1);

    void deleteById(@Param("id") Long var1);

    int update(T var1);

    T getById(@Param("id") Long var1);

    List<T> findAll();
}
