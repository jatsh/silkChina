package com.silkchina.base.exception;


import com.silkchina.base.result.*;

/**
 * The type Rest service exception.
 */
public class RestServiceException extends ServiceException {

    /**
     * Instantiates a new Rest service exception.
     *
     * @param cause the cause
     */
    public RestServiceException(Throwable cause) {
        super(cause);
    }

    /**
     * Instantiates a new Rest service exception.
     *
     * @param errorInfo the error info
     */
    public RestServiceException(Object errorInfo) {

        this.errorCode = 1000;
        this.errorInfo = errorInfo;

        if (errorInfo instanceof Result) {

            Result result = (Result) errorInfo;
            this.setErrorInfo(result.getErrorMessage());
        }
    }

    /**
     * Instantiates a new Rest service exception.
     *
     * @param errorCode the error code
     * @param errorInfo the error info
     */
    public RestServiceException(int errorCode, Object errorInfo) {

        this.errorCode = errorCode;
        this.errorInfo = errorInfo;

        if (errorInfo instanceof Result) {

            Result result = (Result) errorInfo;
            this.setErrorInfo(result.getErrorMessage());
        }
    }

    public int getErrorCode() {
        return this.errorCode;
    }

    public Object getErrorInfo() {
        return this.errorInfo;
    }
}
