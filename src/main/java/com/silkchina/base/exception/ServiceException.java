package com.silkchina.base.exception;

public class ServiceException extends RuntimeException {

    protected int errorCode;

    protected Object errorInfo;

    public ServiceException() {
    }

    public ServiceException(int errorCode, String message) {
        super(message);
        this.errorInfo = message;
    }

    public ServiceException(String message) {
        super(message);
        this.errorCode = 1000;
        this.errorInfo = message;
    }

    public ServiceException(Throwable cause) {
        super(cause);
        this.errorCode = 1000;
        this.errorInfo = cause;
    }

    public ServiceException(int errorCode, Throwable cause) {
        super(cause);
        this.errorInfo = cause;
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
        this.errorCode = 1000;
        this.errorInfo = message;
    }

    public int getErrorCode() {
        return this.errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public Object getErrorInfo() {
        return this.errorInfo;
    }

    public void setErrorInfo(Object errorInfo) {
        this.errorInfo = errorInfo;
    }
}
