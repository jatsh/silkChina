package com.silkchina.base.entity;

import java.io.*;
import java.util.*;

/**
 * @author Wang Jun
 */
public class DataObjectBase implements Serializable {

    private Long id;

    private Date gmtModify;

    private Date gmtCreate;

    private Integer version = Integer.valueOf(0);

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getGmtModify() {
        return gmtModify;
    }

    public void setGmtModify(Date gmtModify) {
        this.gmtModify = gmtModify;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "DataObjectBase{" +
                "id=" + id +
                ", gmtModify=" + gmtModify +
                ", gmtCreate=" + gmtCreate +
                ", version=" + version +
                '}';
    }
}
